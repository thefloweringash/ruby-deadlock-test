#!/usr/bin/env bash

OUTDIR=$2

if [ ! -x $PWD/ruby20 ]; then
	echo "missing ruby"
	exit 1
fi

while true; do
  LD_LIBRARY_PATH=$PWD $PWD/ruby20  $1 | \
  perl -pe 's/(0x[0-9a-fA-F]*)/
            if (defined $PS->{$1}) {
              $PS->{$1}
            } else {
              $PS->{$1} = "p" . $p++;
            }
          /gex' > $OUTDIR/tmp

  H=`sha256 -q $OUTDIR/tmp`
  if [ ! -f $OUTDIR/$H ]; then
      mv $OUTDIR/tmp $OUTDIR/$H
      echo -ne '!'
  else
      echo -ne '.'
  fi
done
