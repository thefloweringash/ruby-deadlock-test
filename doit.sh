#!/usr/bin/env bash

. ~vagrant/env.sh

mkdir -p /vagrant/out

cd $ruby_dir
/vagrant/stress.sh /vagrant/deadlock.rb /vagrant/out
