#!/bin/sh

MIRROR=svn0.us-west.FreeBSD.org

install_packages() {
	echo "Providing packages: $@"
	ps=""
	for p in $@; do
		if ! pkg info -e $p; then
			ps="$ps $p"
		fi
	done
	if [ ! -z "$ps" ]; then
		echo "To install: $ps"
		pkg install -fy $ps
	else
		echo "All packages present"
	fi
}
install_packages ruby20 ruby21 ruby subversion autoconf vim-lite

rm -rf /usr/ports
mkdir -p /usr/ports

svn export http://$MIRROR/ports/head/Mk /usr/ports/Mk
svn export http://$MIRROR/ports/head/Templates /usr/ports/Templates
svn export http://$MIRROR/ports/head/lang/ruby20 /usr/ports/lang/ruby20

patch /usr/ports/lang/ruby20/Makefile /vagrant/ruby-patch-1
cp -f /vagrant/ruby-patch-2 /usr/ports/lang/ruby20/files/patch-thread_debug

MAKE_FLAGS="BATCH=y WRKDIRPREFIX=/tmp WITH_DEBUG=yes DISTDIR=/vagrant/distfiles"

cat <<EOF > ~vagrant/env.sh
ruby_dir=`make $MAKE_FLAGS -C /usr/ports/lang/ruby20 -V BUILD_WRKSRC`
EOF

su -m vagrant -c "sh -c \"/usr/bin/make $MAKE_FLAGS -C /usr/ports/lang/ruby20 clean build\""
